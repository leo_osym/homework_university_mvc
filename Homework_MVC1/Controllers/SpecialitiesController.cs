﻿using Domain;
using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Homework_MVC1.Controllers
{
    public class SpecialitiesController : Controller
    {
        // GET: Specialities
        public ActionResult Index()
        {
            var result = Unit.SpecialitiesRepository.AllItems.ToList();
            return View(result);
        }

        // GET: Specialities/Details/5
        public ActionResult Details(int? id)
        {
            var result = Unit.SpecialitiesRepository.AllItems.FirstOrDefault(x => x.Id == id);
            if (result == null)
                return View("NotFound");
            else
                return View("Details", result);
        }

        // GET: Specialities/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Specialities/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Speciality speciality)
        {
            try
            {
                Unit.SpecialitiesRepository.AddItem(speciality);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Specialities/Edit/5
        public ActionResult Edit(int? id)
        {
            Speciality speciality = Unit.SpecialitiesRepository.AllItems.FirstOrDefault(x => x.Id == id);
            if (speciality != null)
            {
                return PartialView("Edit", speciality);
            }
            return View("Index");
        }

        // POST: Specialities/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, Speciality speciality)
        {
            try
            {
                Unit.SpecialitiesRepository.ChangeItem(speciality);
                //Unit.SpecialitiesRepository.SaveChanges(); 
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Specialities/Delete/5
        public ActionResult Delete(int id)
        {
            Speciality speciality = Unit.SpecialitiesRepository.AllItems.FirstOrDefault(x => x.Id == id);
            if (speciality != null)
            {
                return PartialView("Delete", speciality);
            }
            return View("Index");
        }

        // POST: Specialities/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, Speciality speciality)
        {
            try
            {
                if (speciality != null)
                {
                    Unit.SpecialitiesRepository.DeleteItem(id);
                }
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
