﻿using Domain;
using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Homework_MVC1.Controllers
{
    public class TeachersController : Controller
    {
        // GET: Teachers
        public ActionResult Index()
        {
            var result = Unit.TeachersRepository.AllItems.ToList();
            return View(result);
        }

        // GET: Teachers/Details/5
        public ActionResult Details(int? id)
        {
            var result = Unit.TeachersRepository.AllItems.FirstOrDefault(x => x.Id == id);
            if (result == null)
                return View("NotFound");
            else
                return View("Details", result);
        }

        // GET: Teachers/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Teachers/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Teacher teacher)
        {
            try
            {
                Unit.TeachersRepository.AddItem(teacher);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Teachers/Edit/5
        public ActionResult Edit(int? id)
        {
            Teacher teacher = Unit.TeachersRepository.AllItems.FirstOrDefault(x => x.Id == id);
            if (teacher != null)
            {
                return PartialView("Edit", teacher);
            }
            return View("Index");
        }

        // POST: Teachers/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, Teacher teacher)
        {
            try
            {
                Unit.TeachersRepository.ChangeItem(teacher);
                //Unit.TeachersRepository.SaveChanges(); 
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Teachers/Delete/5
        public ActionResult Delete(int? id)
        {
            Teacher teacher = Unit.TeachersRepository.AllItems.FirstOrDefault(x => x.Id == id);
            if (teacher != null)
            {
                return PartialView("Delete", teacher);
            }
            return View("Index");
        }

        // POST: Teachers/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, Teacher teacher)
        {
            try
            {
                if (teacher != null)
                {
                    Unit.TeachersRepository.DeleteItem(id);
                }
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
