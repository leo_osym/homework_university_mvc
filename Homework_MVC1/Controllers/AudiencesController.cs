﻿using Domain;
using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Homework_MVC1.Models
{
    public class AudiencesController : Controller
    {
        // GET: Audiences
        public ActionResult Index()
        {
            var result = Unit.AudiencesRepository.AllItems.ToList();
            return View(result);
        }

        // GET: Audiences/Details/5
        public ActionResult Details(int? id)
        {
            var result = Unit.AudiencesRepository.AllItems.FirstOrDefault(x => x.Id == id);
            if (result == null)
                return View("NotFound");
            else
                return View("Details", result);
        }

        // GET: Audiences/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Audiences/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind] Audience audience)
        {
            try
            {
                Unit.AudiencesRepository.AddItem(audience);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Audiences/Edit/5
        public ActionResult Edit(int? id)
        {
            Audience audience = Unit.AudiencesRepository.AllItems.FirstOrDefault(x => x.Id == id);
            if(audience!=null)
            {
                return PartialView("Edit", audience);
            }
            return View("Index");
        }

        // POST: Audiences/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, Audience audience)
        {
            try
            {
                Unit.AudiencesRepository.ChangeItem(audience);
                //Unit.AudiencesRepository.SaveChanges(); 
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Audiences/Delete/5
        public ActionResult Delete(int id)
        {
            Audience audience = Unit.AudiencesRepository.AllItems.FirstOrDefault(x => x.Id == id);
            if (audience != null)
            {
                return PartialView("Delete", audience);
            }
            return View("Index");
        }

        // POST: Audiences/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, Audience audience)
        {
            try
            {
                if (audience != null)
                {
                    Unit.AudiencesRepository.DeleteItem(id);
                }
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
