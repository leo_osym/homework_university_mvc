﻿using Domain;
using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Homework_MVC1.Controllers
{
    public class PhonesController : Controller
    {
        // GET: Phones
        public ActionResult Index()
        {
            var result = Unit.PhonesRepository.AllItems.ToList();
            return View(result);
        }

        // GET: Phones/Details/5
        public ActionResult Details(int? id)
        {
            var result = Unit.PhonesRepository.AllItems.FirstOrDefault(x => x.Id == id);
            if (result == null)
                return View("NotFound");
            else
                return View("Details", result);
        }

        // GET: Phones/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Phones/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Phone phone)
        {
            try
            {
                Unit.PhonesRepository.AddItem(phone);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Phones/Edit/5
        public ActionResult Edit(int? id)
        {
            Phone phone = Unit.PhonesRepository.AllItems.FirstOrDefault(x => x.Id == id);
            if (phone != null)
            {
                return PartialView("Edit", phone);
            }
            return View("Index");
        }

        // POST: Phones/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, Phone phone)
        {
            try
            {
                Unit.PhonesRepository.ChangeItem(phone);
                //Unit.PhonesRepository.SaveChanges(); 
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Phones/Delete/5
        public ActionResult Delete(int id)
        {
            Phone phone = Unit.PhonesRepository.AllItems.FirstOrDefault(x => x.Id == id);
            if (phone != null)
            {
                return PartialView("Delete", phone);
            }
            return View("Index");
        }

        // POST: Phones/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, Phone phone)
        {
            try
            {
                if (phone != null)
                {
                    Unit.PhonesRepository.DeleteItem(id);
                }
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
