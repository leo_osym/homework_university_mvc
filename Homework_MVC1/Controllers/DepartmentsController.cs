﻿using Domain;
using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Homework_MVC1.Controllers
{
    public class DepartmentsController : Controller
    {
        // GET: Departments
        public ActionResult Index()
        {
            var result = Unit.DepartmentsRepository.AllItems.ToList();
            return View(result);
        }

        // GET: Departments/Details/5
        public ActionResult Details(int? id)
        {
            var result = Unit.DepartmentsRepository.AllItems.FirstOrDefault(x => x.Id == id);
            if (result == null)
                return View("NotFound");
            else
                return View("Details", result);
        }

        // GET: Departments/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Departments/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Department department)
        {
            try
            {
                Unit.DepartmentsRepository.AddItem(department);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Departments/Edit/5
        public ActionResult Edit(int? id)
        {
            Department department = Unit.DepartmentsRepository.AllItems.FirstOrDefault(x => x.Id == id);
            if (department != null)
            {
                return PartialView("Edit", department);
            }
            return View("Index");
        }

        // POST: Departments/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, Department department)
        {
            try
            {
                Unit.DepartmentsRepository.ChangeItem(department);
                //Unit.DepartmentsRepository.SaveChanges(); 
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Departments/Delete/5
        public ActionResult Delete(int id)
        {
            Department department = Unit.DepartmentsRepository.AllItems.FirstOrDefault(x => x.Id == id);
            if (department != null)
            {
                return PartialView("Delete", department);
            }
            return View("Index");
        }

        // POST: Departments/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, Department department)
        {
            try
            {
                if (department != null)
                {
                    Unit.DepartmentsRepository.DeleteItem(id);
                }
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
