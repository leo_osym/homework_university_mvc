﻿using Domain;
using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Homework_MVC1.Controllers
{
    public class SubjectsController : Controller
    {
        // GET: Subjects
        public ActionResult Index()
        {
            var result = Unit.SubjectsRepository.AllItems.ToList();
            return View(result);
        }

        // GET: Subjects/Details/5
        public ActionResult Details(int id)
        {
            var result = Unit.SubjectsRepository.AllItems.FirstOrDefault(x => x.Id == id);
            if (result == null)
                return View("NotFound");
            else
                return View("Details", result);
        }

        // GET: Subjects/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Subjects/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Subject subject)
        {
            try
            {
                Unit.SubjectsRepository.AddItem(subject);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Subjects/Edit/5
        public ActionResult Edit(int? id)
        {
            Subject subject = Unit.SubjectsRepository.AllItems.FirstOrDefault(x => x.Id == id);
            if (subject != null)
            {
                return PartialView("Edit", subject);
            }
            return View("Index");
        }

        // POST: Subjects/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, Subject subject)
        {
            try
            {
                Unit.SubjectsRepository.ChangeItem(subject);
                //Unit.SubjectsRepository.SaveChanges(); 
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Subjects/Delete/5
        public ActionResult Delete(int id)
        {
            Subject subject = Unit.SubjectsRepository.AllItems.FirstOrDefault(x => x.Id == id);
            if (subject != null)
            {
                return PartialView("Delete", subject);
            }
            return View("Index");
        }

        // POST: Subjects/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, Subject subject)
        {
            try
            {
                if (subject != null)
                {
                    Unit.SubjectsRepository.DeleteItem(id);
                }
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
